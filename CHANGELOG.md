# [2.5.0](https://gitlab.com/dreamer-labs/bashellite/bashellite/compare/v2.4.0...v2.5.0) (2021-06-10)


### Bug Fixes

* Add authentication error checking ([5dde1ce](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/5dde1ce))
* Add use of auth file for registry login ([2e9e238](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/2e9e238))
* Correct logging issue ([6562896](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/6562896))


### Features

* Add use of env.conf ([0c88e09](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/0c88e09))

# [2.4.0](https://gitlab.com/dreamer-labs/bashellite/bashellite/compare/v2.3.2...v2.4.0) (2021-05-03)


### Bug Fixes

* Update dev script to handle multiproject images ([6fc647f](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/6fc647f))
* Update provider_wrapper.sh ([5ec4eed](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/5ec4eed))


### Features

* Add dev script with multiproject support ([ca6086f](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/ca6086f))

## [2.3.2](https://gitlab.com/dreamer-labs/bashellite/bashellite/compare/v2.3.1...v2.3.2) (2020-11-30)


### Bug Fixes

* Update base Dockerfile ([c3418af](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/c3418af))
* Update bootstrap-hos.sh script ([9d35a2c](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/9d35a2c))
* Update bootstrap-service.sh script ([31d6068](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/31d6068))

## [2.3.1](https://gitlab.com/dreamer-labs/bashellite/bashellite/compare/v2.3.0...v2.3.1) (2020-10-17)


### Bug Fixes

* Add freebsd-pkgs provider wrapper ([194eb00](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/194eb00))
* Add freebsd.sh package dev script ([8098cc3](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/8098cc3))
* Add initial files for freebsd-pkgs provider ([7650bc4](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/7650bc4))
* Fix error in freebsd-pkgs provider ([8a77a15](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/8a77a15))
* Move freebsd.sh dev script to dev folder ([0814138](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/0814138))
* Update freebsd provider installer ([b51946c](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/b51946c))

# [2.3.0](https://gitlab.com/dreamer-labs/bashellite/bashellite/compare/v2.2.0...v2.3.0) (2020-08-15)


### Features

* Add docker build bashellite providers ([c8d904f](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/c8d904f))

# [2.2.0](https://gitlab.com/dreamer-labs/bashellite/bashellite/compare/v2.1.0...v2.2.0) (2020-08-15)


### Features

* Add fedora32 vagrant host ([651c5f0](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/651c5f0))

# [2.1.0](https://gitlab.com/dreamer-labs/bashellite/bashellite/compare/v2.0.0...v2.1.0) (2020-08-14)


### Features

* Add centos8+podman env support ([c6bc7f4](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/c6bc7f4))

# [2.0.0](https://gitlab.com/dreamer-labs/bashellite/bashellite/compare/v1.0.2...v2.0.0) (2020-08-11)


### Bug Fixes

* Add shellcheck fixes ([cdfb81b](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/cdfb81b))
* Added .mdlrc with rule exceptions ([45cb0bd](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/45cb0bd))
* change to pip3 ([266d8c0](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/266d8c0))
* Correct git provider repo update error ([eeb09f0](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/eeb09f0))
* Correct issue [#5](https://gitlab.com/dreamer-labs/bashellite/bashellite/issues/5) ([3ece016](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/3ece016))
* correct stale file issue ([d7fb0bd](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/d7fb0bd))
* Corrected markdownlint error ([bfc7b4c](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/bfc7b4c))
* return appropriate error codes ([859931a](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/859931a))
* update git provider ([c0b64b4](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/c0b64b4))
* Update pypi provider_wrapper.sh ([241ad5b](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/241ad5b))


### Features

* Add additional dev scripts ([ff2dccd](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/ff2dccd))
* Add draft dev scripts ([e9fc5f9](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/e9fc5f9))
* add initial files ([0e7b03f](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/0e7b03f))
* Add regex tags to podman provider ([d0828ca](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/d0828ca))
* Bootstrap CI ([ad04fd7](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/ad04fd7))
* Merge remote-tracking branch 'bashellite-providers/master' into merge-providers-repo ([4530f97](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/4530f97))
* Remove installation code ([8f34544](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/8f34544))
* Update podman examples ([ddb1750](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/ddb1750))
* Update podman provider installer ([0e12cbd](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/0e12cbd))
* updated bandersnatch version ([c2c1590](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/c2c1590))
* updated bandersnatch version ([7eeb276](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/7eeb276))
* updated bandersnatch version ([167ffd1](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/167ffd1))


### BREAKING CHANGES

* Prior to this change, the installation method for bashellite used a set
of scripts/code to perform the installation in an automated fashion. This
will soon be supplanted with a different installation method in which
some of the current code will be repurposed/moved, while other parts
will be left behind (deleted). This commit removes the code that will
no longer be required, and temporarily renders bashellite without a
valid installation method until a new commit is merged with that code.

## [1.0.2](https://gitlab.com/dreamer-labs/bashellite/bashellite/compare/v1.0.1...v1.0.2) (2020-01-03)


### Bug Fixes

* Unittest fixups ([26c629e](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/26c629e))

## [1.0.1](https://gitlab.com/dreamer-labs/bashellite/bashellite/compare/v1.0.0...v1.0.1) (2019-12-19)


### Bug Fixes

* update repos ([2dbbc53](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/2dbbc53))

# 1.0.0 (2019-11-05)


### Features

* Bootstrap CI ([4ac15f0](https://gitlab.com/dreamer-labs/bashellite/bashellite/commit/4ac15f0))
