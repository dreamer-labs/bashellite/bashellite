#!/bin/bash

# The passed in input is the path to the base directory to save the packages at
base_dir=${1}

site="http://pkg.freebsd.org"

ABIs="$(cat <<'ENDOFPATH'
FreeBSD:12:amd64
FreeBSD:12:i386
FreeBSD:11:amd64
FreeBSD:11:i386
FreeBSD:10:amd64
FreeBSD:10:i386
FreeBSD:9:amd64
FreeBSD:9:i386
FreeBSD:8:amd64
FreeBSD:8:i386
ENDOFPATH
)"

releases="latest quarterly release_0 release_1 release_2 release_3 release_4"

mkdir -p ${base_dir}

for abi in ${ABIs}; do
    for release in ${releases}; do
        echo "Checking for release: ${abi}/${release}..."
        # First we need to check if the release exists
        response=$(curl -I -s ${site}/${abi}/${release}/)
        resp_code=$(echo ${response} | grep -o "200 OK")
        if [[ ${resp_code} == "200 OK" ]]; then
            echo "Release: ${abi}/${release} found..."
            echo "Checking for pkg.txz for release ${abi}/${release}..."
            # Next we need to check for the pkg.txz package for each release
            respone=$(curl -I -s ${site}/${abi}/${release}/Latest/pkg.txz)
            resp_code=$(echo ${response} | grep -o "200 OK")
            if [[ ${resp_code} == "200 OK" ]]; then
                echo "pkg.txz found, downloading..."
                # We need to download this file
                curl --create-dirs -o ${base_dir}/${abi}/${release}/Latest/pkg.txz -s ${site}/${abi}/${release}/Latest/pkg.txz
            fi
            echo "Checking for pkg.txz.sig for release ${abi}/${release}..."
            # Next we need to check for the pkg.txz.sig file
            respone=$(curl -I -s ${site}/${abi}/${release}/Latest/pkg.txz.sig)
            resp_code=$(echo ${response} | grep -o "200 OK")
            if [[ ${resp_code} == "200 OK" ]]; then
                echo "pkg.txz.sig found, downloading..."
                # We need to download this file
                curl --create-dirs -o ${base_dir}/${abi}/${release}/Latest/pkg.txz.sig -s ${site}/${abi}/${release}/Latest/pkg.txz.sig
            fi
            echo "Checking for meta.conf for release ${abi}/${release}..."
            # Next we need to check for the meta.conf file
            respone=$(curl -I -s ${site}/${abi}/${release}/meta.conf)
            resp_code=$(echo ${response} | grep -o "200 OK")
            if [[ ${resp_code} == "200 OK" ]]; then
                echo "meta.conf found, downloading..."
                # We need to download this file
                curl --create-dirs -o ${base_dir}/${abi}/${release}/meta.conf -s ${site}/${abi}/${release}/meta.conf
            fi
            echo "Checking for meta.txz for release ${abi}/${release}..."
            # Next we need to check for the meta.txz file
            respone=$(curl -I -s ${site}/${abi}/${release}/meta.txz)
            resp_code=$(echo ${response} | grep -o "200 OK")
            if [[ ${resp_code} == "200 OK" ]]; then
                echo "meta.txz found, downloading..."
                # We need to download this file
                curl --create-dirs -o ${base_dir}/${abi}/${release}/meta.txz -s ${site}/${abi}/${release}/meta.txz
            fi
            # Next we need to check if either meta.conf or meta.txz was downloaded
            if [ ! -s ${base_dir}/${abi}/${release}/meta.conf ]; then
                # Here we need to uncompress the meta.txz file if it exists
                if [ -s ${base_dir}/${abi}/${release}/meta.txz ]; then
                    # Here we uncompress the meta.txz file
                    tar -C ${base_dir}/${abi}/${release} -xJ meta.txz
                else
                    # No meta.conf and no meta.txz, so can't continue, skipping this release
                    echo "No meta.conf nor meta.txz found, skipping release ${abi}/${release}..."
                    continue
                fi
            fi

            # Need to recheck for existance of meta.conf in case we tried to extract it from meta.txz
            if [ ! -s ${base_dir}/${abi}/${release}/meta.conf ]; then
                # No meta.conf, so can't continue, skipping this release
                echo "No meta.conf found, skipping release ${abi}/${release}..."
                continue
            fi

            echo "Parsing meta.conf for release ${abi}/${release}..."
            # Here we have a meta.conf to parse...
            IFS=$'\n' 
            for line in $(cat ${base_dir}/${abi}/${release}/meta.conf); do
                if [[ ${line} =~ "packing_format" ]]; then
                    packing_format=${line#packing_format = }
                    packing_format=${packing_format#\"}
                    packing_format=${packing_format%\";}
                fi
                if [[ ${line} =~ "manifests = " ]]; then
                    manifests=${line#manifests = }
                    manifests=${manifests#\"}
                    manifests=${manifests%\";}
                fi
                if [[ ${line} =~ "filesite = " ]]; then
                    filesite=${line#filesite = }
                    filesite=${filesite#\"}
                    filesite=${filesite%\";}
                fi
                if [[ ${line} =~ "manifests_archive = " ]]; then
                    manifests_archive=${line#manifests_archive = }
                    manifests_archive=${manifests_archive#\"}
                    manifests_archive=${manifests_archive%\";}
                fi
                if [[ ${line} =~ "filesite_archive = " ]]; then
                    filesite_archive=${line#filesite_archive = }
                    filesite_archive=${filesite_archive#\"}
                    filesite_archive=${filesite_archive%\";}
                fi
            done
            unset IFS
            filesite_archive_url="${site}/${abi}/${release}/${filesite_archive}.${packing_format}"
            manifests_archive_url="${site}/${abi}/${release}/${manifests_archive}.${packing_format}"
            echo "Checking for ${manifests_archive}.${packing_format} for release ${abi}/${release}..."
            respone=$(curl -I -s ${manifests_archive_url})
            resp_code=$(echo ${response} | grep -o "200 OK")
            if [[ ${resp_code} == "200 OK" ]]; then
                echo "${manifests_archive}.${packing_format} found, downloading..."
                # We need to download this file
                curl --create-dirs -o ${base_dir}/${abi}/${release}/${manifests_archive}.${packing_format} -s ${manifests_archive_url}
            fi
            echo "Checking for existance of ${manifests_archive}.${packing_format}..."
            if [ ! -s ${base_dir}/${abi}/${release}/${manifests_archive}.${packing_format} ]; then
                echo "${manifests_archive}.${packing_format} not found, no packages to download, skipping release ${abi}/${release}..."
                continue
            fi
            packages_list_json=$(tar JxOf ${base_dir}/${abi}/${release}/${manifests_archive}.${packing_format} ${manifests})
            echo "Parsing list of packages for release ${abi}/${release}..."
            IFS=$'\n'
            package_list=( $(echo ${packages_list_json} | jq -c '. | {repopath: .repopath, sum: .sum}') )
            unset IFS
            echo "Getting ${#package_list[@]} packages for release ${abi}/${release}..."
            for line in ${package_list[@]}; do
                pkg_array=( $(echo ${line} | jq -r '.repopath, .sum') )
                found=0
                pkg_path=${pkg_array[0]}
                pkg_sum=${pkg_array[1]}
                echo "Processing package: ${pkg_path}..."
                if [ -s ${base_dir}/${abi}/${release}/${pkg_path} ]; then
                    echo "Package: ${pkg_path} already exists, checking checksum..."
                    file_sum_array=( $(sha256sum ${base_dir}/${abi}/${release}/${pkg_path}) )
                    file_sum=${file_sum_array[0]}
                    if [[ ${file_sum} == ${pkg_sum} ]]; then
                        found=1
                        echo "Package: ${pkg_path} checksums match, skipping..."
                    fi
                fi
                if [[ ${found} == 0 ]]; then
                    echo "Downloading package: ${site}/${abi}/${release}/${pkg_path}..."
                    curl --create-dirs -o ${base_dir}/${abi}/${release}/${pkg_path} -s ${site}/${abi}/${release}/${pkg_path}
                fi
            done
        else
            echo "Release: ${abi}/${release} not found, skipping..."
            continue
        fi
    done
done