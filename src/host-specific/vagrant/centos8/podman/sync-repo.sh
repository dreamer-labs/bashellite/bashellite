#!/usr/bin/env bash

# Validating number of parameter passed to script
if [[ "${#}" != "1" ]]; then
  echo -e "\n[FAIL] Script requires exactly one argument: the name of the repository to sync; exiting." 1>&2;
  exit 1;
fi;

# Sanitizing user input
repo_name="${1//[^a-zA-Z0-9_-]}";
if [[ "${repo_name}" != "${1}" ]]; then
  echo -e "\n[FAIL] Provided repo_name (${1}) contains invalid characters; exiting." 1>&2;
  exit 1;
fi;

# Parsing repo provider, provided a valid repo name was provided as $1 and a repo.conf exists in its directory
unset repo_provider && \
if [[ -f "/etc/bashellite/repos.conf.d/${repo_name}/repo.conf" ]]; then
  while read -r line; do
    if [[ "${line}" =~ ^[[:blank:]]*repo_provider=[^[:blank:]].*$ ]]; then
      # Parsing the value of the first matching parameter on the matching line
      repo_provider="${BASH_REMATCH[0]##*=}";
      # Sanitizing the previously unvalidated repo_provider parameter input
      repo_provider="${repo_provider//[^a-zA-Z0-9_-]}";
    fi;
  done < /etc/bashellite/repos.conf.d/${repo_name}/repo.conf;
else
  echo -e "\n[FAIL] No configured repo found matching given repo name (${repo_name})."
fi;

# Ensuring repo provider value from config file is an actual provider from /opt/bashellite/providers.d/
# If more than one repo_provider was passed in the config file, the last one is considered the value of repo_provider
if [[ ! -d "/opt/bashellite/providers.d/${repo_provider}/" ]]; then
   echo -e "\n[FAIL] No matching provider (${repo_provider}) found for repo (${repo_name}); exiting." 1>&2;
   exit 1; 
fi;

# Assuming the provider repo directory is valid for the valid configured repo,
# attempt to call the pre-built provider container image, if it exists.
# If it does not exist, the script relies on podman's error handling to notify the user.
podman \
  run \
  --rm \
  --userns=keep-id \
  -v /var/log/bashellite:/var/log/bashellite:rw,z \
  -v /etc/bashellite:/etc/bashellite:ro \
  -v /opt/bashellite:/opt/bashellite:ro \
  -v /var/www/bashellite/mirror:/var/www/bashellite/mirror:rw,z \
  bashellite-provider-${repo_provider}:latest \
  /usr/local/bin/bashellite -r ${repo_name};
