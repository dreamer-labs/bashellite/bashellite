#Provider.conf Instructions

This file is a placeholder for provider-specific instructions on how to build-out the `provider.conf` for this provider. Until populated, please reference the `configs/examples/` and `configs/test/` directories for examples of how to configure this provider.

The config file format consists of a listing of FreeBSD:<version>:<platform>

Example listing:

FreeBSD:12:amd64

This will download all the packages for FreeBSD version 12 for the amd64 platform.