bashelliteProviderWrapperPodman() {

  # Set vars based on passed in vars
  config_file="${_r_metadata_tld}/repos.conf.d/${_n_repo_name}/provider.conf"
  env_file="${_r_metadata_tld}/repos.conf.d/${_n_repo_name}/env.conf"
  skopeo_registry_url="${_n_repo_url#http*://}"
  mirror_tld="${_r_mirror_tld}"
  mirror_repo_name="${_n_mirror_repo_name}"
  dryrun=${_r_dryrun}
  authfile=${HOME}/skopeoauth.json

  # Need to check for login env file for registry login information
  username=""
  password=""
  if [ -s ${env_file} ]; then
      utilMsg INFO "$(utilTime)" "Found env_file: ${env_file}..."
      for var in $(cat ${env_file}); do
          if [[ ${var} =~ "my_docker_username=" ]]; then
              utilMsg INFO "$(utilTime)" "Username found!"
              username=${var#my_docker_username=}
          fi
          if [[ ${var} =~ "my_docker_password=" ]]; then
              utilMsg INFO "$(utilTime)" "Password found!"
              password=${var#my_docker_password=}
          fi
      done
  fi

  # Need to attempt to login to registry if username and password found
  if [ ${#username} -gt 0 ] && [ ${#password} -gt 0 ]; then
      utilMsg INFO "$(utilTime)" "Login credentials found, creating authfile..."
      skopeo login -u=${username} -p=${password} --authfile=${authfile} ${skopeo_registry_url}
  fi

  image_name_array=()

  for line in $(cat ${config_file}); do
    utilMsg INFO "$(utilTime)" "Processing line: ${line}"
    orig_line=${line}
    repo_line=${line%%:*}
    # Here we need to check the image name to see if it doesn't use a username.  If if doesn't, we will use the
    # default docker username 'library'
    IFS=$'/\n'
    repo_line_array=( ${repo_line} )
    unset IFS
    if [[ ${#repo_line_array[@]} == 1 ]]; then
        repo_line="library/${repo_line}"
    fi
    image_name=${repo_line//\//__}
    
    # Check to see if tags are listed
    tag_index=0
    tag_index=`expr index "${line}" ':'`
    tags_found=""
    tags_found="${line:${tag_index}}"

    if [[ ${tag_index} == 1 || ${tags_found} == "" ]]; then
        utilMsg WARN "$(utilTime)" "Invalid image/tag format found: ${orig_line}, skipping..."
    else
        if [[ ${tag_index} == 0 ]]; then
            # Didn't find any tags, so setting tag to 'latest'
            utilMsg INFO "$(utilTime)" "Tags not found, using 'latest' tag"
            tags_found="latest"
        else
            # Tags found
            utilMsg INFO "$(utilTime)" "Tags found"
        fi

        # Create tags array to cycle through
        IFS=$',\n'
        tags_array=( ${tags_found} )
        unset IFS

        # Here we need to make a back up of the tag array, then reset the tag array
        # and go through the copy.  If we find a regular tag, we add it to the new tag array.
        # If we find a regex, we get a list of all the tags using the dockertags helper scripts and
        # compare them against the regex.  On a match, add those tags to the new tag array.

        orig_tags_array=${tags_array[@]}
        new_tags_array=()

        tag_list=""

        for each_tag in ${orig_tags_array[@]}; do
            if [[ ${each_tag:0:1} != "/" ]]; then
                new_tags_array+=" ${each_tag}"
            else
                # Possible regex tag, check for ending delimiter
                if [[ ${each_tag: -1} != "/" ]]; then
                    utilMsg WARN "$(utilTime)" "Invalid regex tag format found: ${each_tag}, skipping..."
                else
                    # Here we have a good regex, now we download all the tags for this image if they haven't already been downloaded
                    if [[ ${tag_list} == "" ]]; then
                        # tag_list=$(${exec_dir}/dockertagsv1.sh ${repo_username}/${image_name})
                        # Here we replace the use of dockertagsv1.sh with Auth token checking and use to access the 
                        # list of tags in a v2 compliant rapid way
                        # First, do a header check to see where to get auth token
                        token=""
                        auth_info=""
                        # Here we get the auth info by making a request with an invalid token to the docker endpoint
                        while read line; do
                            if [[ ${line} =~ Www-Authenticate ]]; then
                                # We need to get auth token using info from the Www-Authenticate header...
                                auth_info=${line//*Bearer }
                                IFS=$',\n'
                                auth_info=( ${auth_info} )
                                unset IFS
                                # Here we clean up the parameters received from the previous request
                                realm=${auth_info[0]#realm=}
                                realm=${realm//\"/}
                                param1=${auth_info[1]//\"}
                                param1=${param1//:/%3A}
                                param1=${param1//\//%2F}
                                param1=${param1//$'\r'}
                                param2=${auth_info[2]//\"}
                                param2=${param2//:/%3A}
                                param2=${param2//\//%2F}
                                param2=${param2//$'\r'}
                                url="${realm}?${param1}&${param2}"
                                # Here we get a valid token that is valid for 300 sec
                                token=$(curl -s "${url}" | jq -r '.token')
                                utilMsg INFO "$(utilTime)"echo "Token used: $token"
                                break
                            fi
                        done < <(curl -I -s -H "Authorization: Bearer ${token}" "${_n_repo_url}/v2/${repo_line}/tags/list");

                        # Here we use the token to get the list of tags for the image being requested
                        tag_list=$(curl -s --request 'GET' -H "Authorization: Bearer ${token}" ${_n_repo_url}/v2/${repo_line}/tags/list | jq -r '.tags[]')

                    fi
                    # Next we grep the tag_list against the regex (each_tag)
                    for tag in ${tag_list}; do
                        tmpregextag=${each_tag#/}
                        regex_tag=${tmpregextag%/}
                        echo ${tag} | egrep -E "${regex_tag}" > /dev/null
                        if [[ ${?} == 0 ]]; then
                            new_tags_array+=" ${tag}"
                        fi
                    done
                fi
            fi
        done

        # Next we will cycle through each tag, check for an already saved file, and use skopeo to inspect the saved image and the online image and
        # compare image creation dates
        for each_tag in ${new_tags_array[@]}; do
            utilMsg INFO "$(utilTime)" "Processing image: ${repo_line}:${each_tag}..."
            save_loc="${mirror_tld}/${mirror_repo_name}"
            image_file_name="${image_name}___${each_tag}.tar"
            
            # Check to see if a corresponding file exists for the image with this tag
            if [ -s ${save_loc}/${image_file_name} ]; then
                utilMsg INFO "$(utilTime)" "Previously saved file found: ${save_loc}/${image_file_name}..."
                utilMsg INFO "$(utilTime)" "Checking saved image creation timestamp..."
                local_timestamp=$(skopeo inspect docker-archive:${save_loc}/${image_file_name} | jq -r '.Created')
                utilMsg INFO "$(utilTime)" "Checking online image creation timestamp..."
                if [ -s ${authfile} ]; then
                    utilMsg INFO "$(utilTime)" "Using auth file for login credentials to get image timestamp..."
                    online_timestamp=$(skopeo inspect --authfile ${authfile} docker://${skopeo_registry_url}/${repo_line}:${each_tag} | jq -r '.Created')
                    error_code=${?}
                else
                    utilMsg INFO "$(utilTime)" "Using anonymous login to get image timestamp..."
                    online_timestamp=$(skopeo inspect docker://${skopeo_registry_url}/${repo_line}:${each_tag} | jq -r '.Created')
                    error_code=${?}
                fi
                if [[ ${error_code} != 0 ]]; then
                    if [[ ${error_code} == 1 ]]; then
                        utilMsg FAIL "$(utilTime)" "Authorization required!  Create an environment file called /etc/bashellite/repo.conf.d/${_n_repo_name}/env.conf"
                        utilMsg FAIL "$(utilTime)" "with the following format:"
                        utilMsg FAIL "$(utilTime)" "my_docker_username=username"
                        utilMsg FAIL "$(utilTime)" "my_docker_password=password"
                        # Need to clean out possible old left behind tmp files located in /var/tmp that may have been left behind by errored
                        # skopeo runs
                        rm -rf /var/tmp/docker-tarfile-blob*
                        unset skopeo_registry_url;
                        exit 1
                    else
                        utilMsg FAIL "$(utilTime)" "Unknown error encountered.  See error log..."
                        # Need to clean out possible old left behind tmp files located in /var/tmp that may have been left behind by errored
                        # skopeo runs
                        rm -rf /var/tmp/docker-tarfile-blob*
                        unset skopeo_registry_url;
                        exit 2
                    fi
                fi
                
                # Next with we compare the local timestamp to the online timestamp and save if online timestamp is newer
                if [[ (${local_timestamp} == ${online_timestamp}) || (${local_timestamp} > ${online_timestamp}) ]]; then
                # Newest image already saved, skipping...
                utilMsg INFO "$(utilTime)" "Saved image: ${repo_line}:${each_tag} is already current, skipping..."
                continue
                fi
            fi

            # Need to download and save the image
            repo_image="${skopeo_registry_url}/${repo_line}:${each_tag}"
            utilMsg INFO "$(utilTime)" "Saving tag: ${each_tag} for image: ${repo_line}"
            utilMsg INFO "$(utilTime)" "Command: skopeo copy docker://${repo_image} docker-archive:${save_loc}/${image_file_name}"
            if [[ ${dryrun} == "" ]]; then
                # Only save if not a dry run
                # # Check if file already exists, if it does, check to see if it is current
                if [ -s ${save_loc}/${image_file_name} ]; then
                    # Need to remove the old file before we can save the new one
                    rm -f ${save_loc}/${image_file_name}
                fi
                if [ -s ${authfile} ]; then
                    utilMsg INFO "$(utilTime)" "Using auth file for login credentials to download image..."
                    skopeo copy --authfile ${authfile} docker://${repo_image} docker-archive:${save_loc}/${image_file_name}
                    error_code=${?}
                else
                    utilMsg INFO "$(utilTime)" "Using anonymous login to download image..."
                    skopeo copy docker://${repo_image} docker-archive:${save_loc}/${image_file_name}
                    error_code=${?}
                fi
                if [[ ${error_code} != 0 ]]; then
                    if [[ ${error_code} == 1 ]]; then
                        utilMsg FAIL "$(utilTime)" "Authorization required!  Create an environment file called /etc/bashellite/repo.conf.d/${_n_repo_name}/env.conf"
                        utilMsg FAIL "$(utilTime)" "with the following format:"
                        utilMsg FAIL "$(utilTime)" "my_docker_username=username"
                        utilMsg FAIL "$(utilTime)" "my_docker_password=password"
                        # Need to clean out possible old left behind tmp files located in /var/tmp that may have been left behind by errored
                        # skopeo runs
                        rm -rf /var/tmp/docker-tarfile-blob*
                        unset skopeo_registry_url;
                        exit 1
                    else
                        utilMsg FAIL "$(utilTime)" "Unknown error encountered.  See error log..."
                        # Need to clean out possible old left behind tmp files located in /var/tmp that may have been left behind by errored
                        # skopeo runs
                        rm -rf /var/tmp/docker-tarfile-blob*
                        unset skopeo_registry_url;
                        exit 2
                    fi
                fi
            fi
        done
    fi
  done

  # Need to clean out possible old left behind tmp files located in /var/tmp that may have been left behind by errored
  # skopeo runs
  rm -rf /var/tmp/docker-tarfile-blob*

  # Need to remove authfile
  rm -rf ${authfile}

  unset skopeo_registry_url;

}
