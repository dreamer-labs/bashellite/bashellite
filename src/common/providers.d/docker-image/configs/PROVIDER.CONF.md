#Provider.conf Instructions

This file is a placeholder for provider-specific instructions on how to build-out the `provider.conf` for this provider. Until populated, please reference the `configs/examples/` and `configs/test/` directories for examples of how to configure this provider.

The config file format consists of either an image to download per line or an image plus a list of tags to download per line.  When specifying tags the format is: <image_name>:tag1,tag2,...,tagN

When specifying the tag, you can either provide a specific tag or use a posix extended regex for the tag to be compared with the full list of tags.  When specifying a regex tag the format is: /<regex>/

Example image with tag plus regex tag:

ubuntu:18.04,/19.*/

This will download the 18.04 tag image for ubuntu plus any tags that contain 19 followed by any number of characters.