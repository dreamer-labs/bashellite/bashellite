#Provider.conf Instructions

The provider.conf file acts as a whitelist for the packages to download.  It is formatted with each package name on a separate line.

Note:  If the provider.conf file is empty, this provider will query the pypi index specified in the repo.conf file for the list of packages to download.
