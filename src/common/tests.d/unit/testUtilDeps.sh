#!/usr/bin/env bash

# shellcheck disable=SC2034,SC1091

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

oneTimeSetUp() {
  local lib_dir
  lib_dir=$(realpath "${MY_DIR}/../../bases.d/.data/usr/local/lib")

  # shellcheck source=usr/local/lib/util-libs.sh
  source ${lib_dir}/util-libs.sh;
}

testUtilDeps() {

  # Tests fail condition for if "which" is not in path
  assertSame "[FAIL] Dependency (which) missing!" "$(which() { return 1; }; utilDeps foo 2>&1)";
  assertSame "1" "$(which() { return 1; }; utilDeps foo 2>/dev/null; echo "${?}")";

  # Tests fail condition for if requested_function_input contains invalid characters
  assertSame "[FAIL] Requested dependency or array name contains invalid characters; exiting." "$(utilDeps f00b@r! 2>&1)";
  assertSame "1" "$(utilDeps f00b@r! 2>/dev/null; echo "${?}")";

  # Tests success condition for valid array names and single deps
  assertSame "0" "$(utilDeps shunit2 &>/dev/null; echo "${?}")";
  assertSame "0" "$(unittest_array=( shunit2 ); utilDeps unittest_array &>/dev/null; echo "${?}")";

  # Tests fail condition for if a dependency is not in path
  assertSame "[FAIL] Dependency (my_fake_dep) missing!" "$(utilDeps my_fake_dep 2>&1)";
  assertSame "1" "$(utilDeps my_fake_dep 2>/dev/null; echo "${?}")";

}

# shellcheck source=/dev/null
source "$(which shunit2)";

