#!/usr/bin/env bash

# shellcheck disable=SC1091

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

oneTimeSetUp() {
  lib_dir=$(realpath "${MY_DIR}/../../bases.d/.data/usr/local/lib")
  config_dir="${lib_dir}/../../../../../etc/bashellite";

  # shellcheck source=usr/local/lib/util-libs.sh
  source ${lib_dir}/util-libs.sh;
  # shellcheck source=usr/local/lib/bashellite-libs.sh
  source ${lib_dir}/bashellite-libs.sh;
}

testInitFlags() {

    rslt=$(bashelliteSetup -m "www.example.com" -c "${config_dir}" \
           -r 'abc1234-repo' -p 'abc123' -d -a 2>&1);

    assertContains "${rslt}" "See testBashelliteSetup.sh usage below"

}

testRelativeConfig() {

  rslt=$(bashelliteSetup -c "../${config_dir}" -d 2>&1);

  assertFalse $?
  assertContains "${rslt}" "Absolute paths only, please;"

}

testRelativeMirror() {

  rslt=$(bashelliteSetup -m "../tmp" -c "${config_dir}" -r 'abc1234-repo' -p 'abc123' -d -a 2>&1);

  assertFalse $?
  assertContains "${rslt}" "Absolute paths only, please;"

}

testSetMirror() {

  rslt=$(bashelliteSetup -m "/tmp/bashellite/mirror" -c "${config_dir}" \
         -r 'abc1234-repo' -p 'abc123' -d -a 2>&1);

  assertFalse $?
  assertContains "${rslt}" "Absolute paths only, please;"

}

testRelativeProviders() {

  rslt=$(bashelliteSetup -m "/tmp/bashellite/mirror" -p "../tmp" -c "${config_dir}" -r 'abc1234-repo' -d 2>&1);
  assertFalse $?
  assertContains "${rslt}" "Absolute paths only, please;"

}

testSetProviders() {

  rslt=$(bashelliteSetup -p "/tmp/bashellite/providers" \
         -m "/tmp/bashellite/mirror"  -c "${config_dir}" \
         -r 'abc1234-repo' -d 2>&1);

  assertFalse $?
  assertNotContains "${rslt}" "Absolute paths only, please;"

}

testRepoTest() {

  rslt=$(bashelliteSetup -p "/tmp/bashellite/providers" \
         -m "/tmp/bashellite/mirror" -c "${config_dir}" \
         -r 'test' -d 2>&1);

  # assertFalse $?
  assertNotContains "${rslt}" 'Bashellite requires at least one valid repository.'
}

testBadRepo() {

  rslt=$(bashelliteSetup -p "/tmp/bashellite/providers" \
         -m "/tmp/bashellite/mirror"  -c "${config_dir}" \
         -r 'test_not_exists' -d 2>&1);

  assertFalse $?
  assertContains "${rslt}" 'Bashellite requires at least one valid repository.'
}

testAllAndRepo() {

  rslt=$(bashelliteSetup -p "/tmp/bashellite/providers" \
         -m "/tmp/bashellite/mirror"  -c "${config_dir}" \
         -r 'test_not_exists' -a -d 2>&1);

  assertFalse $?
  assertContains "${rslt}" 'The flags -a and -r are mutually exclusive. Use one or the other; exiting.'
}

testInvalidRepoName() {

  rslt=$(bashelliteSetup -r 'abc&123' 2>&1);
  assertContains "${rslt}" "Requested repo_name contains invalid characters"

}

testUnsetVars() {

  rslt=$(_r_mirror_tld='TEST1' bashelliteSetup 2>&1);

  assertNotContains "$var" "TEST1"

}

testFailedUnset() {
  _r_mirror_tld='TEST1'

  # mock out unset to cause failure condition
  rslt=$( unset(){ return 1; };  bashelliteSetup 2>&1; );

  assertFalse $?
  assertContains "${rslt}" "Unable to unset variable (_r_mirror_tld)"

  # Make sure it wasn't able to unset _r_mirror_tld='TEST1'
  assertEquals "${_r_mirror_tld}" "TEST1"

}

testHelpMsg() {

  rslt=$(bashelliteSetup -h 2>&1;)

  assertTrue $?
  assertContains "${rslt}" 'Usage'

}

testBadOpt() {

  rslt=$(bashelliteSetup -x 2>&1;)

  assertFalse $?
  assertContains "${rslt}" 'Usage'

}

# shellcheck source=/dev/null
source "$(which shunit2)";
