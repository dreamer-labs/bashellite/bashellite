#!/usr/bin/env bash

# shellcheck disable=SC1091

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

oneTimeSetUp() {

  BIN_DIR=$(realpath "${MY_DIR}/../../bases.d/.data/usr/local/bin")
  local lib_dir
  lib_dir=$(realpath "${MY_DIR}/../../bases.d/.data/usr/local/lib")

  # shellcheck source=usr/local/lib/util-libs.sh
  source ${lib_dir}/util-libs.sh;
  # shellcheck source=usr/local/lib/bashellite-libs.sh
  source ${lib_dir}/bashellite-libs.sh;
  # shellcheck source=usr/local/bin/bashellite.sh
  source "${BIN_DIR}/bashellite.sh";

}

testBashelliteExec() {

  rslt=$(bashellite_main 2>&1);

  assertContains "$rslt" "Log directory (/var/log/bashellite) does not exist; exiting.";

}

# shellcheck source=/dev/null
source "$(which shunit2)";
