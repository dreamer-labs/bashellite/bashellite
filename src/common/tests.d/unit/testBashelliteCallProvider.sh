#!/usr/bin/env bash

# shellcheck disable=SC1091

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


oneTimeSetUp() {
  lib_dir=$(realpath "${MY_DIR}/../../bases.d/.data/usr/local/lib")
  config_dir="${lib_dir}/../../../../../etc/bashellite";

  # shellcheck source=usr/local/lib/util-libs.sh
  source "${lib_dir}/util-libs.sh";
  # shellcheck source=usr/local/lib/bashellite-libs.sh
  source "${lib_dir}/bashellite-libs.sh";
}

testNoRepo() {

  rslt=$(
    bashelliteCallProvider 2>&1;
  );

  assertFalse $?
  assertContains "${rslt}" 'Failed to pass a value for variable (_n_repo_name) to function'

}

testMetadata_tldRepo() {

  rslt=$(
    _n_repo_name='TEST' bashelliteCallProvider 2>&1;
  );

  assertFalse $?
  assertContains "${rslt}" 'Failed to pass a value for variable (_r_metadata_tld) to function'

}

test_Bad_Repo() {

  rslt=$(
    _n_repo_name='bad' _r_metadata_tld="${config_dir}" bashelliteCallProvider 2>&1;
  );

  assertFalse $?
  assertContains "${rslt}" ' No metadata for (bad) present in repo metadata directory'

}

test_Test_Repo() {

  rslt=$(
    _n_repo_name='test' _r_metadata_tld="${config_dir}" bashelliteCallProvider 2>&1;
  );
  # This should probably fail but passes
  assertFalse $?
  assertContains "${rslt}" 'Failed to source provider_wrapper for provider'


}

fixture_test_repo() {
  if [[ -d "$1" ]] && [[ "$1" =~ /tmp/.* ]]; then
    rm -rf "$1"
  else
    tmpdir=$(mktemp -d);
    mkdir -p "$tmpdir/repos.conf.d/test"
    touch "$tmpdir/repos.conf.d/test/provider.conf"
    touch "$tmpdir/repos.conf.d/test/repo.conf"
    echo "$tmpdir"
  fi
}

testFailSanitizeRepoURL() {
   # Sanitizes repo_url :261

   repoPath=$(fixture_test_repo)

   rslt=$(
     _n_repo_name='test' _r_metadata_tld="$repoPath" _n_repo_url='' bashelliteCallProvider 2>&1;
   );
   assertFalse $?
   assertContains "${rslt}" 'repo_url not set in repo.conf file for (test)'

   fixture_test_repo "$repoPath"
}

testFailSanitizeProvider() {
  # # Sanitizes repo_provider :269

   repoPath=$(fixture_test_repo)
   echo -e "repo_url=\"example.com\"\n" > "$repoPath/repos.conf.d/test/repo.conf"

   rslt=$(
     _n_repo_name='test' _r_metadata_tld="$repoPath" _n_repo_url='' bashelliteCallProvider 2>&1;
   );
   assertFalse $?
   assertContains "${rslt}" 'repo_provider not set in repo.conf file for (test)'

   fixture_test_repo "$repoPath"

}

testSourceProvider() {
  # Source Provider :275

   repoPath=$(fixture_test_repo)
   echo -e "repo_url=\"example.com\"\nrepo_provider=\"test\"\n" > "$repoPath/repos.conf.d/test/repo.conf"
   mkdir -p "$repoPath/providers/test"
   echo -e "temp_func() { echo 'Ran temp_func'; };\n echo '--test bash script--'" > "$repoPath/providers/test/provider_wrapper.sh"
   rslt=$(
     _r_providers_tld="$repoPath/providers" _n_repo_name='test' _r_metadata_tld="$repoPath" _n_repo_url='' bashelliteCallProvider 2>&1;
   );
   assertTrue $?
   assertContains "${rslt}" '--test bash script--'
   fixture_test_repo "$repoPath"

}

testFailProviderWrapper() {
  # Failed to source provider_wrapper : 281
  repoPath=$(fixture_test_repo)
  echo -e "repo_url=\"example.com\"\nrepo_provider=\"test\"\n" > "$repoPath/repos.conf.d/test/repo.conf"
  mkdir -p "$repoPath/providers/test"
  echo -e "\n echo '--test bash script--'" > "$repoPath/providers/test/provider_wrapper.sh"
  rslt=$(
    _r_providers_tld="$repoPath/providers" _n_repo_name='test' _r_metadata_tld="$repoPath" _n_repo_url='' bashelliteCallProvider 2>&1;
  );
  assertFalse $?
  assertContains "${rslt}" 'Failed to execute provider_wrapper function  for provider (test);'
  fixture_test_repo "$repoPath"
}

# shellcheck source=/dev/null
source "$(which shunit2)";
