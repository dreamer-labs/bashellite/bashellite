#!/usr/bin/env bash

tests_tld="$(realpath "$(dirname "${BASH_SOURCE[0]}")" )";
reports_tld="${PWD}/.coverage";
test_type="${1}";

if [[ "${#test_type}" != "0" ]] && [[ -d "${tests_tld}/${test_type}/" ]]; then
  echo "[INFO] Requested test type (${test_type}) is valid; proceeding.";
else
  echo "[FAIL] Requested test type (${test_type}) is invalid; exiting." 1>&2;
  exit 1;
fi;
 
if [[ -d "${reports_tld}" ]]; then
  { echo -e "\n[INFO] Removing old coverage results.";
    rm -fr "${reports_tld}"; } || \
  { echo -e "\n[INFO] Unable to remove previous coverage results directory (${reports_tld}); exiting." 1>&2;
    exit 1; };
fi;

mapfile -t < <(compgen -G "${tests_tld}/${test_type}/*.sh");
if [[ "${#MAPFILE[@]}" == "0" ]]; then \
  echo -e "\n[WARN] No matching test files found for test type (${test_type}); exiting." 1>&2; \
  exit 0; \
fi;

for test_file in "${MAPFILE[@]}"; do
  echo -e "\n[INFO] Running tests in test file (${test_file}).";
  bash "${test_file}" || \
  { echo -e "\n[FAIL] Failed to execute all requested tests in test file (${test_file}); exiting." 1>&2; 
    exit 1; };
  echo -e "\n[INFO] Calculating coverage for files sourced by test file (${test_file}).";
  kcov \
    --exclude-path /opt/shunit2/,"${tests_tld}"/ \
    --bash-method=DEBUG \
    "${reports_tld}" \
    "${test_file}" || \
  { echo -e "\n[FAIL] Failed to calculate coverage for file sourced by test file (${test_file}); exiting." 1>&2;
    exit 1; };
done;

echo -e "\n[INFO] Printing summary report of coverage results.";
jq '.' "${reports_tld}"/kcov-merged/coverage.json || \
echo -e "\n[WARN] Failed to print generated summary report." 1>&2;
